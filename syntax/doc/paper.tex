% This is LLNCS.DEM the demonstration file of
% the LaTeX macro package from Springer-Verlag
% for Lecture Notes in Computer Science,
% version 2.2 for LaTeX2e
%
\documentclass[english,runningheads]{llncs}
%
\usepackage[ngerman]{babel}
\usepackage{makeidx}
\usepackage{graphicx}
%
\begin{document}
\pagestyle{headings}  % switches on printing of running heads
%
\title{Classical Sindarin Syntax}
\subtitle{Machine Language Processing WS2007/2008}
\titlerunning{Sindarin Syntax}
%
\author{Thomas Zink \and Andreas Engl \and Wolfgang Miller}
\tocauthor{Thomas Zink, Andreas Engl, Wolfgang Miller(University of Konstanz)}
%
\institute{University of Konstanz\\
\email{$\left\{ thomas.zink, andreas.engl, wolfgang.miller \right\}$ @uni-konstanz.de}}
%
\maketitle              % typeset the title of the contribution
%
\begin{abstract}
Sindarin is a constructed language invented by the author and linguist J.R.R. Tolkien. It is the language spoken by the Elves. Though the number of complete sindarin languages is rather limited\cite{SALO} enough information is available to derive syntactic rules and build a Sindarin grammar. This project aims at modelling the basics of Classical Sindarin grammar in LFG using Xerox XLE system. In addition it interfaces to Xerox XFST to lookup morphological information which has already been implemented in another project. For more information about Classical Sindarin morphology see the project documentation\cite{MORPH}. The project is mainly based on information provided by David Salo's "A Gateway to Sindarin"\cite{SALO}.
\end{abstract}
%
\section{System Overview}
%
Figure~\ref{fig:overview} shows an overview of the system implementation. It consists of three components which are further discussed in more detail. 
%
\begin{figure}[ht!]
	\centerline{\includegraphics[width=\linewidth]{system}}
	\caption{System Overview}
	\label{fig:overview}
\end{figure}
%
\subsection{Tokenizer}
%
The tokenizer takes textual input and generates tokens that can be further processed and analysed by the morphological analyser. It performs the following tasks:
%
\begin{itemize}
	\item all characters are cast to lower-case.
	\item accents are removed.
	\item binding articles (like \textit{i-}) are split from nouns.
	\item numbers are identified and isolated
	\item special characters like punctuation characters are isolated.
\end{itemize}
%
Tokenization removes ambiguities and simplifies the creation and maintenance of lexicon files for the morphological analyser and the \textbf{xle} grammar. Though xle provides a basic tokenizer that is suitable for english and german grammars it is not sufficient for parsing Sindarin. The main reason is the existence of articles that bind directly to the corresponding noun. They have to be split and provided as a distinct token for analysis. Otherwise, all possible permutations of binding articles and nouns would have to be present in the \textbf{xle} and \textbf{lexc} lexicons. Another reason are the numerous accentuations present in Sindarin. Table~\ref{tab:vowels} shows the Sindarin vowels and their accentuations. Accentuations undergo different morphological changes, sometimes to another accent (like long to stressed) sometimes disappearing completely. Again to prevent the \textbf{xle} lexicon to degenerate the accentuations are completely removed and normalized to their unaccented vowel. Since the version of \textbf{xfst} used for this project cannot deal with unicode characters (and even the unicode character set would not suffice to represent all Sindarin sounds) the whole system is designed to process only 7bit ascii encoded files. Using other encodings can lead to erroneous results.

The tokenizer can be tested using an input file or the \textbf{echo} command.
%
\begin{verbatim}
# echo "i-t^ol acharn." | tokenize sindarin.tok.fst
i-
tol
acharn
.
\end{verbatim}
%
\begin{table*}
	\centering
	\begin{tabular}[h]{@{}p{2cm}|p{0.5cm}@{}p{0.5cm}@{}p{0.5cm}@{}p{0.5cm}@{}p{0.5cm}@{}p{0.5cm}@{}p{0.5cm}@{}p{0.5cm}@{}}
			\hline \hline
			Short & a & e & i & o & u & y \\
			Long & 'a & 'e & 'i & 'o & 'u & 'y \\
			Stressed & \^{}a & \^{}e & \^{}i & \^{}o & \^{}u & \^{}y \\
			\hline \hline
	\end{tabular}
	\caption{Sindarin Accentuation\label{tab:vowels}}
\end{table*}
%
\subsection{Morphological Analyser}
%
The morphological analyser takes tokenized input and returns the stem or lemma of the token and some morphological tags that bear information about the changes of the stem.
Sindarin morphology has already been discussed and implemented in \textbf{xfst} for another project \cite{MORPH}. See section~\ref{sec:morph} for an explanation of the implementation of the morphological analyser. 
%
\subsection{LFG}
%

%
\section{Sindarin Morphology}\label{sec:morph}
%
The implementation used is based on the work done in a former project. However, the original implementation was not suitable for integration with \textbf{xle} for various reasons. First, the system features a combined morphology of articles and nouns. A seperate lookup of articles or even noun forms affected by articles is not possible. This drastically affects the size of the \textbf{xle} lexicon which would have to list every combination of nouns and articles. Second, the sindarin writing system, called Tengwar, is phonologic, that is every sound has it's own symbol. Due to the lack of enough characters in the ascii character set some sounds had to be represented using a set of two and in rare occasions even three characters. To closely resemble Tengwar, these sets have been implemented as multicharacter symbols, resulting in a single state in the state machine. As it turns out, this raises problems in evaluating the morphology, since all multicharacter symbols are automatically returned as tags by \textbf{lookup}. Thus, the correct stem cannot be retrived. Figure~\ref{fig:lupold} shows an example of a lookup in the original morphological analyser. Clearly this would fail to generate a correct input for \textbf{xle}.
%
\begin{figure}
\tt \small
\begin{verbatim}
# echo "i meraid" |tokenize sindarin.tok.fst |lookup -flags mb sindarin.old.fst 

9 input symbols processed.
i	i	+?

meraid	meraid	+?
\end{verbatim}
\caption{Lookup in the original sindarin morphological analyser}
\label{fig:lupold}
\end{figure}
%

For these reasons the Sindarin morphological analyser had to be completely rewritten. The new system renounces the use of multicharacter symbols to represent Sindarin sounds. It allows the lookup of articles independent of nouns and their affected forms.  Figure~\ref{fig:lupnew} shows the same lookup used earlier in the new implemented morphological analyser. 
%
\begin{figure}
\tt \small
\begin{verbatim}
# echo "i meraid" |tokenize sindarin.tok.fst |lookup -flags mu sindarin.morph.fst 

9 input symbols processed.
i	i	+cDet+Pl

meraid	barad	+Def+cNoun+Pl
\end{verbatim}
\caption{Lookup in the new implementation of the  sindarin morphological analyser}
\label{fig:lupnew}
\end{figure}
%
As can be seen \textbf{lookup} now outputs a pair of stems and morphological tags for all input tokens. This output is suitable for further processing in \textbf{xle}.
%
\subsection{Morphological TAGS}
%
Table~\ref{tab:tags} shows the available tags used in the morphological analyser. 
%
\begin{table*}
	\centering
	\begin{tabular}[h!]{@{ }p{1.5cm}|p{2cm}|p{4.5cm}|p{3cm}}
			\hline \hline
			TAG & POS & meaning & example \\
			\hline
			 +Det & determiner & regular determiner & in+Det+Pl\\
			 +cDet & determiner & determiner co-ocurring with Nouns starting with a consonant & i+cDet+Pl \\
			 +iDet & determiner & determiner co-ocurring with Nouns starting with an i sound & ir+iDet+Sg \\
			 +Indef & nouns & noun is in an indefinit state, ie bears no article & amon+Indef+Noun+Sg \\
			 +Def & nouns & noun is in a definit state, ie bears an article & amon+Def+Noun+Sg \\
			 +Sg & determiner, nouns & singular person & i-+Det+Sg  amon+Def+Noun+Sg \\
			 +Pl & determiner, nouns & plural person & in+Det+Pl amon+Def+Noun+Pl\\
			 +Gen & determiner, nouns & genitive case & en+Det+Gen edhel+Def+Noun+Gen \\
			 +Noun & nouns & regular noun & amon+Indef+Noun+Sg \\
			 +cNoun & nouns & noun starting with a consonant, affect plural determiner & barad+Def+cNoun+Sg\\
			 +iNoun & nouns & noun starting with an i sound, affect singular determiner & \^{}idh+Def+iNoun+Sg\\
			 +pNoun & nouns & plural noun, i.e. the base form is plural & n\^{}el+Indef+pNoun+Pl\\
			 +Norm & nouns & the noun is normalized, that is, at least one vowel accentuation has been removed & idh+Norm+Def+iNoun+Sg\\
			 +Ath & nouns & plural ath-suffix & hw\^{}in+Indef+cNoun+Ath\\
			\hline \hline
	\end{tabular}
	\caption{Tags with their part-of-speech and meaning\label{tab:tags}}
\end{table*}
%
The tags are set to ensure an unambiguous legal combination of determiners and nouns. Some noun forms can be identical in different cases while allowing only the presence or absence of a specific article. Figure~\ref{fig:luptag0} shows an example. 
%
\begin{figure}[h!]
\tt \small
\begin{verbatim}
# echo "i-edhel" |tokenize sindarin.tok.fst |lookup -flags mu sindarin.morph.fst 

8 input symbols processed.
i-	i-	+Det+Sg

edhel	edhel	+Def+Noun+Sg
edhel	edhel	+Def+Noun+Gen
edhel	edhel	+Indef+Noun+Sg
\end{verbatim}
\caption{The significance of tags for morphological analysis}
\label{fig:luptag0}
\end{figure}
%
As can be seen, the form \textit{edhel} can occur in three cases. However, together with the singular article \textit{i-} there is only one legal combination which is the first in the example. Figure~\ref{fig:luptag1} shows what happens if the hyphen is replaced by a space character.
%
\begin{figure}[h!]
\tt \small
\begin{verbatim}
# echo "i edhel" |tokenize sindarin.tok.fst |lookup -flags mu sindarin.morph.fst 

8 input symbols processed.
i	i	+cDet+Pl

edhel	edhel	+Def+Noun+Sg
edhel	edhel	+Def+Noun+Gen
edhel	edhel	+Indef+Noun+Sg
\end{verbatim}
\caption{The significance of tags for morphological analysis}
\label{fig:luptag1}
\end{figure}
%
Again, the token \textit{edhel} can occur in three cases. However, the plural article \textit{i} is tagged with \textit{cDet} meaning it can only co-occur with nouns starting with a consonant that are tagged with \textit{cNoun}. The noun phrase \textit{i edhel} thus is illegal and would be rejected by the grammar. These conditions can be modeled in \textbf{xle} and a careful choice of tags and condition reduces the development and maintenance effort.
%
\section{Sindarin Grammar}\label{sec:gram}
Sindarin is a consistently \textit{head-left} driven language. That is, in any phrase the \textit{head} will be on the left and their modifying complements will follow on the right. The following sections describe phrases in more detail.
%
\section{Noun Phrases}
%
A simple noun phrase consists only of a single noun, pronoun or name. A noun phrase can also consist of a simple noun phrase followd by other noun phrases.
%
\begin{verbatim}
	NP --> { N | PN | NM }
	hmhmhm can't figure out how to solve recursive NP* ?
\end{verbatim}
%
%
% ---- Bibliography ----
%
\begin{thebibliography}{2}
%
\bibitem {SALO}
Salo, D.:
A Gateway to Sindarin.
The University of Utah Press, Salt Lake City 2004.
%
\bibitem {MORPH}
Zink, T., Engl A.:
Sindarin Morphology.
Finite-State Morphology SS 2007, University of Konstanz.
%
\end{thebibliography}

\end{document}
