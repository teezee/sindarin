# ========================================================= 
# sindarin.xfst
# @content: creates the sindarin morphological analyser
# @author: Thomas Zink, Andreas Engl
# =========================================================
# Usage: source [ThisFile] or xfst -f [ThisFile]
# ========================================================= 
clear stack
# ========================================================= 
# DEFINITIONS
# ========================================================= 
### Vowels
define Vshort [ a | e | i | o | u | y ];
define Vlong  [ {'a} | {'e} | {'i} | {'o} | {'u} | {'y} ];
define Vstressed [ {^a} | {^e} | {^i} | {^o} | {^u} | {^y} ];
define Vdiph  [ {ae} | {ai} | {au} | {aw} | {ei} | {oe} | {oi} | {ui} ];
define Vowels [ Vshort | Vlong | Vstressed | Vdiph ];
### Consonants
define Cmono [ t | p | c | d | b | g | h | f | v | m | n | r | s | l | w ];
define Cdi   [ {gw} | {ng} | {rh} | {lh} | {ch} | {chw} | {hw} | {th} | {dh} | {ph} ];
define Consonants [ Cmono | Cdi ];
# ========================================================= 
# LEXICON
# ========================================================= 
read lexc < sindarin.lexc
define lexnet
# ========================================================= 
# RULES
# ========================================================= 
read regex < sindarin.regex;
define rulnet
# ========================================================= 
# COMPILE
# ========================================================= 
read regex lexnet .o. rulnet;
save stack sindarin.morph.fst
# EOF